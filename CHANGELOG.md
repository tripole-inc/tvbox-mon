# Change Log

Changelog for TVbox-mon.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 2022-04-11

### Changed
 - Updated the documentation.

## 2022-04-10

### Added
 - This changelog file.

### Fixed
 - Removed redundant condition for clean exit.

### Changed

## [0.1] - 2022-04-10

### Added
 - Initial commit.

