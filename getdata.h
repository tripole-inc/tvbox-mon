/*!\file
    Function declaration for the data collection functionality.
*/

#ifndef GETDATA_H
#define GETDATA_H

struct netstat_data;
struct diskstat_data;
struct diskstat_table;

/*! Reads the config file (if it exists). */
int config_read(const char *conf_file, const char *prg_name,
                char* argv[], char *v_arg, char **vargp);

/*! Determines the state for a network interface (up/down). */
int iface_getstate(const char *iface_name);

/*! Detects if there are any removable USB devices attached. */
int usb_getstate(void);

/*! Reads the temperature from a sysfs node. */
int temp_getstate(void);

#endif
