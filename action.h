/*!\file
    Function declarations for the higher level LED blinking actions (tasks).
*/

#ifndef ACTION_H
#define ACTION_H

/*! Action to test all available icons on the VFD. */
int test_action(int period_msec, struct vfd_iconprops *vfd_icn,
                FILE *fd_ledon, FILE *fd_ledoff);

/*! Action to detect/signal addition/removal of external device on usb bus. */
int usb_action(struct vfd_iconprops *vfd_icn, FILE *fd_ledon, FILE *fd_ledoff);

/*! Action to detect/signal up/down of ethernet/wireless network interface. */
int iface_action(char *iface_str, struct vfd_iconprops *vfd_icn,
                 FILE *fd_ledon, FILE *fd_ledoff);

/*! Action to read temperature and signal alarm if necessary. */
int temp_action(int warn_temp, int dangr_temp,
                struct vfd_iconprops *vfd_icn, FILE *fd_ledon, FILE *fd_ledoff);

#endif
