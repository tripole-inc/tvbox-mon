/*!\file
    The functions defined in this file perform the higher level actions of VFD
    icon/LED blinking.
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "def.h"
#include "action.h"
#include "getdata.h"
#include "ledctrl.h"
#include "utility.h"

extern int debug_level;

/*! Action to test all available icons on the VFD. */
int test_action(int period_msec, struct vfd_iconprops *vfd_icn,
                FILE *fd_ledon, FILE *fd_ledoff)
{
    struct vfd_iconprops vfd_loc = *vfd_icn;
    int scene_musec = period_msec, worktime_musec = 0;

    /* Put the icons on, one after another, each one cancelling the previous. */
    if (debug_level > 1)
        fprintf(stderr,
                "%s: Test 1: turning on icons, one after another.\n", __func__);

    worktime_musec += led_blink(1, 1, &vfd_loc.eth_state, vfd_loc.eth_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    worktime_musec += led_blink(1, 1, &vfd_loc.wifi_state, vfd_loc.wifi_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    worktime_musec += led_blink(1, 1, &vfd_loc.usb_state, vfd_loc.usb_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    worktime_musec += led_blink(1, 1, &vfd_loc.play_state, vfd_loc.play_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    worktime_musec += led_blink(1, 1, &vfd_loc.pause_state, vfd_loc.pause_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    worktime_musec += led_blink(1, 1, &vfd_loc.alarm_state, vfd_loc.alarm_str,
                                fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    /* Light 'em all up! */
    if (debug_level > 1)
        fprintf(stderr, "%s: Test 2: turning on all icons.\n",__func__);

    worktime_musec += icons_onoff(1, &vfd_loc, fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    usleep(scene_musec);
    worktime_musec += scene_musec;

    /* Put icons for eth, wifi, usb, play, pause and alarm in 0 (off) state. */
    if (debug_level > 1)
        fprintf(stderr, "%s: Test 3: turning off all icons.\n",__func__);

    worktime_musec += icons_onoff(0, &vfd_loc, fd_ledon, fd_ledoff);
    usleep(scene_musec);
    worktime_musec += scene_musec;

    *vfd_icn = vfd_loc;

    return worktime_musec;
}

/*! Action to detect/signal addition/removal of external device on USB bus. */
int usb_action(struct vfd_iconprops *vfd_icn, FILE *fd_ledon, FILE *fd_ledoff)
{
    struct vfd_iconprops vfd_loc = *vfd_icn;
    int worktime_musec = 0;

    int exists_extusbdev = usb_getstate();

    worktime_musec +=
        led_onoff(exists_extusbdev, &vfd_loc.usb_state, vfd_loc.usb_str,
                  fd_ledon, fd_ledoff);

    *vfd_icn = vfd_loc;

    if (debug_level > 1)
        fprintf(stderr, "%s: Info: Function work time is "\
                "%d (ms)\n", __func__, (int)(worktime_musec/1000));

    return worktime_musec;
}

/*! Action to detect/signal up/down of ethernet/wireless network interface. */
int iface_action(char *iface_str, struct vfd_iconprops *vfd_icn,
                 FILE *fd_ledon, FILE *fd_ledoff)
{
    struct vfd_iconprops vfd_loc = *vfd_icn;
    int worktime_musec = 0, iface_state = 0;
    
    iface_state = iface_getstate(iface_str);

    if (debug_level > 1)
        fprintf(stderr, "%s: State of iface %s (i.e. nominal speed): "\
              "%d Mbit/s\n", __func__, iface_str, iface_state);

    if (iface_state > 0)
        iface_state = 1; /* For eth0, iface_state = linkspeed (nominal). */

    if (strcmp(iface_str, "eth0") == 0) {
        worktime_musec +=
            led_onoff(iface_state, &vfd_loc.eth_state, vfd_loc.eth_str,
                      fd_ledon, fd_ledoff);
    } else {
        worktime_musec +=
            led_onoff(iface_state, &vfd_loc.wifi_state, vfd_loc.wifi_str,
                      fd_ledon, fd_ledoff);
    }

    vfd_loc = *vfd_icn;

    if (debug_level > 1)
        fprintf(stderr, "%s: Info: Function work time is "\
                "%d (ms)\n", __func__, (int)(worktime_musec/1000));

    return worktime_musec;
}


/*! Action to read temperature and signal alarm if necessary. */
int temp_action(int warn_temp, int dangr_temp,
                struct vfd_iconprops *vfd_icn, FILE *fd_ledon, FILE *fd_ledoff)
{
    struct vfd_iconprops vfd_loc = *vfd_icn;
    int worktime_musec = 0, temp_state = 0;

    temp_state = temp_getstate();

    if ((temp_state > warn_temp) && (temp_state <= dangr_temp)) {
        worktime_musec +=
            led_blink(1, 1, &vfd_loc.alarm_state, vfd_loc.alarm_str,
                      fd_ledon, fd_ledoff);
    } else {
        if (temp_state > dangr_temp) {
            worktime_musec +=
            led_onoff(1, &vfd_loc.alarm_state, vfd_loc.alarm_str,
                      fd_ledon, fd_ledoff);
        }
    }

    vfd_loc = *vfd_icn;

    if (debug_level > 1)
        fprintf(stderr, "%s: Info: Function work time is "\
                "%d (ms)\n", __func__, (int)(worktime_musec/1000));

    return worktime_musec;
}
