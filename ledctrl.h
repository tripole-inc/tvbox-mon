/*!\file
    Functions to perform low level LED control, such as LED on/off or blink.
*/

#ifndef LEDCTRL_H
#define LEDCTRL_H

/*! Turns an icon/LED on or off. */
int led_onoff(int onoff, int *indc_state, char *indc_icon,
              FILE *fd_ledon, FILE *fd_ledoff);

/*! Makes one or several on-off blinks of one icon/LED. */
int led_blink(int noblinks, int onoff, int *indc_state, char *indc_icon,
              FILE *fd_ledon, FILE *fd_ledoff);

/*! Turn on or off all icons on the display except the colon. */
int icons_onoff(int onoff, struct vfd_iconprops *vfd_icn,
                FILE *fd_ledon, FILE *fd_ledoff);

#endif
