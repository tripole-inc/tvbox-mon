/*!\file

tvbox-mon
---------

By Tripole, 2022-04-05.

Program to monitor the USB bus, Ethernet and wireless interfaces, and signal
the status of these using the LED or VFD (vacuum fluorescent) display on a TV
box running Linux. Communication with the Linux kernel is done via the OpenVFD
driver (module) which must be available.

The program was developed on a Tanix TX3-mini (mfg. by Oranth, w/ s905w SoC,
2GB mem. and FD628 display controller) but should be easy to adapt to other
similar devices. The most important device specific parameter/variable is the
struct definition vfd_iconprops (in def.h) and the instantiation of it below.

The code is released under the Gnu General Public License v3.0.

*/

/* --------------------------- C/Linux Includes ---------------------------- */

#include <stdlib.h>       /* Basic programming functionality, like exit().  */
#include <fcntl.h>        /* File handling (like open/close) of files.      */
#include <unistd.h>       /* Basic file read/write (like sysfs files).      */
#include <stdio.h>        /* stdin, stdout, stderr, and the fprint() fam.   */
#include <string.h>       /* String handling functions, like strcpy() etc.  */
#include <errno.h>        /* Defines errno and gives explicit error codes.  */
#include <sys/ioctl.h>    /* For ioctl etc.                                 */
#include <signal.h>       /* For interrupt signals (e.g. Ctrl-C (SIGINT)).  */
#include <getopt.h>       /* getopt(), optarg and opterr  */
#include <libgen.h>       /* For the basename function.   */
#include <linux/stddef.h> /* The macro for NULL. */

/* ---------------------------- Local includes ----------------------------- */

#include "def.h"      /* Constants and macro definitions.   */
#include "action.h"   /* Higher level (blink sequence) actions. */
#include "getdata.h"  /* Data gathering (e.g. reading of proc/, sysfs/).   */
#include "ledctrl.h"  /* Low level LED control (on/off, blink etc.).       */
#include "utility.h"  /* Utility functions. */

/* -------------------------------- Globals -------------------------------- */

int debug_level = 0; /*!< {0,1,2} When debug_level=0 verbosity takes over. */
int verbosity_level = 1; /*!< {0,1,2} Used at command line and in config.  */
int exit_signal = 0; /*!< {0} Used in the signal handler. */

/*! opterr is initialized to 0 in main() (subtle point, see getopt.h). */
extern int opterr;

/* extern int errno; (Should not be explicitly declared, see man errno(3).)  */

/* -------------------------- In-module functions -------------------------- */

/*! A simple (but POSIX compliant, no less) interrupt signal handler. */
static void sig_handler(int sig_num){
    if (sig_num == SIGHUP || sig_num == SIGINT || sig_num == SIGTERM)
        exit_signal = sig_num; /* List signals/numbers with $kill -l */
}

static void init_signals(struct sigaction sigact){
    sigact.sa_handler = sig_handler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigaction(SIGHUP, &sigact, (struct sigaction *)NULL);
    sigaction(SIGINT, &sigact, (struct sigaction *)NULL);
    sigaction(SIGTERM, &sigact, (struct sigaction *)NULL);
}

/* --------------------------------- Main ---------------------------------- */

/*!

    The general workflow in main() is simple:

    1. Parse arguments from command line or config file (using config_read()
       and getopt()).

    2. Verify that openvfd is available in the kernel (e.g. as a module).

    3. Enter perpetual loop depending on selected cycle (program), interrupted
       only by a change in exit_signal (detected by sig_handler()).

*/

int main(int argc, char *argv[])
{
    int ret = -1;

    /* The following defaults are used in various places of the code below. */
    char vfd_cycle = VFDDEFAULTCYCLECODE;
    int period_musec = 1000*VFDDFLTPERIODMISEC; /* Default for prog. cycle. */
    FILE *fd_led_on, *fd_led_off; /* fd's to sysfs nodes for icon on/off msg. */
    struct vfd_iconprops vfd_icons =
        {"eth", "wifi", "usb", "play", "pause", "alarm", "colon",
         0, 0, 0, 0, 0, 0, 0};
    int warning_temp = WARNINGTEMP;
    int danger_temp = DANGERTEMP;

    /* Set up elementary interrupt signal handling. */
    struct sigaction led_sigact;
    init_signals(led_sigact);

    /* Prepare for argument (options) parsing. (An option consists of a switch
       (e.g '-p') followed by a value (e.g. '2000'), i.e. two arguments.) */
    int c_arg = 0;
    char v_arg[MAXARGWORDS*MAXARGSTRLEN] = {'\0'};

    char prog_name[LINELEN] = {'\0'};
    strcpy(prog_name, basename(argv[0]));
    int progname_len = strlen(prog_name);

    int p_argc = 0;
    char **p_argv;
    p_argv = calloc(1, (progname_len+MAXARGWORDS)*sizeof(char *));

    const char config_file[] = CONFFILE;

    char opts_str[LINELEN] = {'\0'};

    /* The overall logic for arguments (options) to tvbox-mon is as follows:
       If there are command line options, use these. Otherwise try to read the
       config file, and if options are found use these. As a last resort, use
       default values (from def.h). */
    if (argc == 1)  /* No cmd. line arguments: We need to try to read config. */
        c_arg = config_read(config_file, prog_name, argv, v_arg, &*p_argv);

    if (c_arg > 0) {    /* We found a config file with at least one argument. */
        p_argc = c_arg +1; /* c_arg doesn't include the calling prog. name. */
        for (int i=1; i<p_argc; i++) {
           strcat(opts_str, p_argv[i]);
           strcat(opts_str, " ");
        }
    } else {
        p_argc = argc;
        p_argv = argv;
    }

    /* Do options parsing, using arguments from command line or config file. */
    opterr = 0;

    int opt;

    char usage_msg[40*LINELEN] = USAGEMSG;
    /* Order of options arguments in the list defines search/parse order
       (cf. man 3 getopt) and execution order in the case statements is
       important. */
    while ((opt = getopt(p_argc, p_argv, "v:c:p:w:d:ht")) != -1)
        switch (opt) {
            case 'v':
                verbosity_level = mystr2num(&optarg[0]);
                break;
            case 'c':
                vfd_cycle = optarg[0];
                if ((vfd_cycle != 'n') && (vfd_cycle != 'u') &&
                    (vfd_cycle != 'b') && (vfd_cycle != 'q'))
                    vfd_cycle = 'b';

                if (vfd_cycle == 'q')
                    period_musec = 1000000;
                break;
            case 'p':
                period_musec = 1000*mystr2num(&optarg[0]);
                if (period_musec < 1000*VFDMINPERIODMISEC)
                     period_musec = 1000*VFDMINPERIODMISEC;
                break;
            case 'w':
                warning_temp = mystr2num(&optarg[0]);
                break;
            case 'd':
                danger_temp  = mystr2num(&optarg[0]); /* Most important. */
                if (danger_temp < warning_temp)
                    warning_temp = danger_temp -1;
                break;
            case 't':
                period_musec = 1000000;
                vfd_cycle = 't';
                break;
            case '?':
            case 'h':
                fprintf(stderr, "This is %s, version %s (binary name %s).\n",
                                PROGNAMESTR, VERSIONSTR, prog_name);
                fprintf(stderr, "%s\n", SYNOPSISSTR);
                fprintf(stderr, "Location of (optional) config file: %s\n",
                                CONFFILE);
                fprintf(stderr, "If no options are given, the conf. file "\
                                "(if it exists) will be parsed for opts.\n\n");

                /* If needed, insert more info here like so: */
                /* strcat(usage_msg, XTRAUSAGEMSG); */
                fprintf(stderr, usage_msg, prog_name);

                exit(0);
                break;
            default:
                fprintf(stderr, "Doh. We should never end up here.");
        }

    if (debug_level > 0) {
        fprintf(stderr, "%s: Info: compiled with debug_level = %d > 0; "\
                        "verbosity set to 2.\n",
                        __func__, debug_level);
        verbosity_level = 2;
    }

    /* This is only information but it is important so we print it anyway. */
    if (verbosity_level > 0) {
        if (verbosity_level > 1) {
                char cmd_line[LINELEN] = {'\0'};
                for (int i=0; i<argc; i++) {
                    strcat(cmd_line, argv[i]);
                    strcat(cmd_line, " ");
                }
            if (c_arg > 0) {
                fprintf(stderr, "%s: Program call (options from conf. file):\n"\
                                "     %s%s\n", prog_name, cmd_line, opts_str);
            } else {
                fprintf(stderr, "%s: Program call: \n     %s\n",
                                prog_name, cmd_line);
            }
            fprintf(stderr, "%s: Temperature alarm settings (deg C): "\
                            "warning: %d, danger: %d\n",
                            prog_name, warning_temp, danger_temp);
        }
        fprintf(stderr, "%s: starting, process ID %d, ", prog_name, getpid());
        fprintf(stderr, "press Ctrl-C (or send SIGINT) to exit.\n");
    }

    /*! A working openvfd sys node is necessary for all that follows. */
    if (access(OPENVFDPATH, F_OK) != -1) {
        fd_led_on  = fopen(OPENVFDPATH"/led_on", "w");
        fd_led_off = fopen(OPENVFDPATH"/led_off", "w");
        setlinebuf(fd_led_on); /* Writes are flushed by \n. */
        setlinebuf(fd_led_off);
        if (fd_led_on == NULL || fd_led_off == NULL) {
            fprintf(stderr, "Could not access openvfd led nodes, exiting.\n");
            exit(1);
        }
        if (verbosity_level > 1)
            fprintf(stderr, "%s: Found working sysfs node for openvfd, ok.\n",
                            prog_name);
        } else {
            fprintf(stderr, "Could not find syfs node for openvfd, exiting.\n");
            exit(1);
    }

    /*! Initialize the VFD/icons to off state. (This is the default inside
        this code; outside the (assumed) state is LEDDEFAULTEXTERNSTATE.) */
    icons_onoff(0, &vfd_icons, fd_led_on, fd_led_off);

    if (verbosity_level > 1)
        fprintf(stderr, "%s: Finished startup, entering main (perpetual) "\
                        "loop.\n", prog_name);

    /* Main loop. */
    while (exit_signal == 0) {
        int wrk_musec = 0, idletime_musec = 0;
        switch (vfd_cycle) {
                case 't':
                    test_action(period_musec, &vfd_icons,
                                fd_led_on, fd_led_off);
                    break;
                case 'n':
                    wrk_musec += iface_action("eth0", &vfd_icons,
                                              fd_led_on, fd_led_off);
                    wrk_musec += iface_action("wlan0", &vfd_icons,
                                              fd_led_on, fd_led_off);
                    wrk_musec += temp_action(warning_temp, danger_temp,
                                             &vfd_icons, fd_led_on, fd_led_off);

                    if (wrk_musec < period_musec) {
                        idletime_musec = period_musec - wrk_musec;
                        usleep(idletime_musec);
                    }

                    if (debug_level > 1)
                        fprintf(stderr, "%s: Info: Loop work time is "\
                                        "%d ms, idle time is %d ms\n",
                                __func__, (int)(wrk_musec/1000),
                                          (int)(idletime_musec/1000));
                    break;
                case 'u':
                    wrk_musec += usb_action(&vfd_icons, fd_led_on, fd_led_off);
                    wrk_musec += temp_action(warning_temp, danger_temp,
                                             &vfd_icons, fd_led_on, fd_led_off);
                    if (wrk_musec < period_musec)
                        usleep(period_musec - wrk_musec);
                    break;
                case 'b':
                    wrk_musec += usb_action(&vfd_icons, fd_led_on, fd_led_off);
                    wrk_musec += iface_action("eth0", &vfd_icons,
                                              fd_led_on, fd_led_off);
                    wrk_musec += iface_action("wlan0", &vfd_icons,
                                              fd_led_on, fd_led_off);
                    wrk_musec += temp_action(warning_temp, danger_temp,
                                             &vfd_icons, fd_led_on, fd_led_off);
                    if (wrk_musec < period_musec)
                        usleep(period_musec - wrk_musec);
                    break;
                case 'q':
                        usleep(period_musec);
                    break;
            /*    default: */
            /* Define some simple default case here... */
        }
    } /* while */

    /* Leave icons/LED in default external state. */
    icons_onoff(ICONSDEFAULTEXTERNSTATE, &vfd_icons, fd_led_on, fd_led_off);
    fclose(fd_led_on);
    fclose(fd_led_off);
    ret = 0;

    if (debug_level > 1) {
        fprintf(stderr, "\n%s: Info: Received signal %d (%s), exiting.\n",
                        __func__, exit_signal, strsignal(exit_signal));
    }

    return ret;
}
