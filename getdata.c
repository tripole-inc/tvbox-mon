/*!\file
    The functions define here handle all forms of data collection; reading of
    config file, reading in the sysfs file system, and preprocessing of data
    obtained from there.
*/

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "def.h"
#include "getdata.h"
#include "utility.h"

extern int debug_level;

/*!
    The config file is located, and if it exists it is read and preparsed.
    The result is packaged together with the calling program name and returned.
*/
int config_read(const char *conf_file, const char *prg_name,
                    char* argv[], char *v_arg, char **vargp)
{
    int c_arg = 0;

    if (access(conf_file, R_OK) == 0) {
        if (debug_level > 1) {
                fprintf(stderr, "%s: Info: Found (readable) config file %s:\n",
                                __func__, conf_file);
        }

        FILE *fd_cfile = fopen(conf_file, "r");

        if (fd_cfile == NULL) {
            if (debug_level > 0)
                    fprintf(stderr, "%s: Error: Failed to open stream %s (%s)",
                                    __func__, conf_file, strerror(errno));

            exit(1);
        }

        /* The config file is (first) read into a (preallocated) array of
           char of length MAXARGWORDS*MAXARGSTRLEN in the form of MAXARGWORDS
           consecutive slots (words) of length MAXARGSTRLEN. Each slot is
           intiated with null chars. Then, as long as there are command line
           argumentss left to process, each such argument is read into one
           slot. (A command line argument, or option, can be defined in at
           least wo ways, see below.) */

        for (int i=0; i<MAXARGWORDS; i++) {
            for (int j=0; j<MAXARGSTRLEN; j++) {
                v_arg[i*MAXARGSTRLEN+j] = '\0';
            }
        }

        int cstr_idx = -1, in_line_idx = 0, in_cstr_idx = 0;
        int space_num = 0, is_writing = 0;

        char cfile_buf[LINELEN] = {'\0'};
        char *cfile_line;

        /* fgets() appends a trailing null '\0' to the read stream/file. */
        cfile_line = fgets(cfile_buf, LINELEN, fd_cfile);

        /* A command line can be parsed in at least two ways, or modes: In both
           modes the first string on the command line which does not contain a
           space is the calling program name, so the modes only differ in the
           treatment of the following strings. In the first parsing mode, the
           'options' mode, any of the subsequent strings which is initiated by
           a dash "-" and ends just before another dash (or a newline) is
           considered as an option (string). In the second mode, the
           'arguments' mode, all strings of non-space characters (and thus
           ending just before a space or a newline) is considered as an
           argument. For example, in the 'options' mode the command line
           "tvbox-led -v 1 -p 2500" has three options, "tvbox-led", "-v 1" and
           "-p 2500". In the 'arguments' mode the same line has five arguments,
           "tvbox-led", "-v", "1", "-p" and "2500". Apparently, C's argv is
           supposed to be parsed in the 'arguments' mode. */

        static const int cmd_line_is_argsmode = 1;

        char conf_char;

        while ((in_line_idx < LINELEN) &&
                   (cfile_line[in_line_idx] != '\0') &&
                       (cfile_line[in_line_idx] != '\n')) {

            conf_char = cfile_line[in_line_idx];

            if (cmd_line_is_argsmode == 1) {  /* Using 'arguments' mode. */

                if (conf_char != ' ') {
                    if (is_writing == 0) {  /* Beginning of an argument. */
                        cstr_idx++;
                        in_cstr_idx = 0;
                        is_writing = 1;
                    } else {                /* Processing an argument. */
                        in_cstr_idx++;
                    }
                } else {
                    is_writing = 0;
                }

            } else {  /* Using 'options' parsing mode. */

                if (conf_char == '-') {    /* Found new substring. */
                    cstr_idx++;
                    in_cstr_idx = 0;
                    space_num = 0;
                    is_writing = 1;
                } else if (conf_char == ' ') {
                      if (space_num == 0) {  /* An opt. has exactly one space.*/
                           in_cstr_idx++;
                           space_num++;
                        } else {
                           is_writing = 0;
                       }
                } else {
                    in_cstr_idx++;
                }

            }

            if (is_writing == 1)
                v_arg[cstr_idx*MAXARGSTRLEN+in_cstr_idx] = conf_char;

            in_line_idx++;
        } /* while config parsing loop */

        c_arg = (cstr_idx > -1) ? cstr_idx +1 : 0 ;

        if (fclose(fd_cfile) == -1) {
            if (debug_level > 0)
                fprintf(stderr, "%s: Error: Failed to close stream %s (%s)\n",
                                __func__, conf_file, strerror(errno));

            exit(1);
        }

        if (debug_level > 1) {
            fprintf(stderr, "%s: Info: Processed configuration file %s\n",
                            __func__, conf_file);
            fprintf(stderr, "%s: Info: Found %d option substrings.\n",
                            __func__, c_arg);

            char sub_str[MAXARGSTRLEN] = {'\0'};
            for (int i=0; i<c_arg; i++) {
                for (int j=0; j< MAXARGSTRLEN; j++) {
                    sub_str[j] = v_arg[i*MAXARGSTRLEN+j];
                }
                fprintf(stderr, "%s: Info: Substring num %d: %s\n",
                                __func__, i+1, sub_str);
            }
        }

        if (c_arg > 0) {
            vargp[0] = argv[0];
            for (int i=0; i<c_arg; i++)
                vargp[i+1] = &v_arg[i*MAXARGSTRLEN];

        }

    } else if (debug_level > 1) {
          fprintf(stderr, "%s: Info: Missing config file %s",
                           __func__, conf_file);
    }
    
    return c_arg;
}

/*!
   The state for a network interface (eth0/wlan0) is obtained by reading data
   from standard locations/nodes in the sysfs system (under /sys/class/net).
*/
int iface_getstate(const char *iface_name)
{

/**
    The operating state is marked differently in sysfs for wired and wireless
    network interfaces. A wired interface, such as eth0, has opstate="up" as
    soon as the interface is (software) configured (i.e. up) and there is a
    physical connection, even if it has no IP address assigned (and thus cannot
    pass traffic). (A physical connection exists if a cable is connected, which
    is signaled by a '1' in the carrier state in the sysfs interface for eth0.)
    A wireless interface, such as wlan0, on the other hand has opstate="down"
    even when it is configured but has no IP address. A wireless interface has
    opstate="up" precisely when it (is up and) has an IP address. For these
    reasons we employ the following definition: An ethernet interface is up
    precisely when both the relevant software interface is up (i.e. visible in
    the 'ip l' or 'ifconfig' listing) and the link (wired/wireless) is
    established and is capable of sending and receiving data. (The above is my
    current understanding of things but it may not be fully accurate.)
*/

/*! First, read the operating state of the interface. We do this in order to
    make sure that any further readings, such as carrier (eth0) and bit rates
    (wlan0) will be available and make sense. */
    char *iface_opstate_file;

    if (strcmp(iface_name, "eth0") == 0) {
        iface_opstate_file = ETH0STATEPATH"/operstate";
    } else {
        iface_opstate_file = WLAN0STATEPATH"/operstate";
    }

    /* iface is "up", "down" or "unknown", so we use two char +null. */
    char state_str[3] = {'\0', '\0', '\0'};
    FILE *fd_opstate = fopen(iface_opstate_file, "r");

    if (fd_opstate != NULL) {
        if (fgets(state_str, 3, fd_opstate) != NULL) {
            fclose(fd_opstate);
        } else {
            if (debug_level > 0)
                fprintf(stderr, "%s: Warn: Failed to read %s (%s)\n",
                                 __func__, iface_opstate_file, strerror(errno));
        }
    } else {
        if (debug_level > 0)
            fprintf(stderr, "%s: Warn: Failed to open %s (%s)\n",
                            __func__, iface_opstate_file, strerror(errno));
    }

    /* Both char arr in comparison here are two char +null (i.e. strings). */
    /* Note: We treat a possible "unknown" (or garbage) state as "down". */
    int state_num = (strcmp(state_str, "up") == 0) ? 1 : 0 ;

    /*! Then, read the network interface link speed. (The return value to this
        here function is extd_statenum, which is synonymous with linkspeed). */

    /* (Note: According to our definition above for the state of an interface,
       the link speed is not used to determine if eth0 is up (this is based on
       cable connect/disconnect) but we collect the linkspeed for eth0 anyway.
       For wlan0, on the other hand, link speed (bit rate) is used to determine
       the up/down state. */
    int linkspeed = 0, extd_statenum = 0;

    if (state_num == 1) {

        if (strcmp(iface_name, "eth0") == 0) {

            /* First, check for cable connect. */
            char *iface_carrier_file = ETH0STATEPATH"/carrier";
            FILE *fd_carrier = fopen(iface_carrier_file, "r");
            char carrier_str[2] = {'\0', '\0'};
            int carrier_num = 0;

            if (fd_carrier != NULL) {
                if (fgets(carrier_str, 2, fd_carrier) != NULL) {
                    fclose(fd_carrier);
                } else {
                    if (debug_level > 0)
                        fprintf(stderr, "%s: Warn: Failed to read %s (%s)\n",
                                __func__, iface_carrier_file, strerror(errno));
                }
            } else {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Warn: Failed to open %s (%s)\n",
                            __func__, iface_carrier_file, strerror(errno));
            }

            if (strcmp(carrier_str, "1") == 0)
                carrier_num = 1;

            if (debug_level > 1)
                fprintf(stderr, "%s: Info: eth0 cable connect indicator: %d\n",
                                 __func__, carrier_num);

            /* If carrier_num != 1 the exit value of this here fcn. is 0. */
            if (carrier_num == 1) {

                char *iface_linkspeed_file = ETH0STATEPATH"/speed";
                FILE *fd_linkspeed = fopen(iface_linkspeed_file, "r");
                /* Form of link speed string is "10\n", "1000\n" or "1000\n". */
                char linkspeed_str[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};

                if (fd_linkspeed != NULL) {
                    if (fgets(linkspeed_str, 6, fd_opstate) != NULL) {
                        fclose(fd_linkspeed);
                    } else {
                        if (debug_level > 0)
                            fprintf(stderr,
                                    "%s: Warn: Failed to read %s (%s)\n",
                                    __func__, iface_linkspeed_file,
                                    strerror(errno));
                    }
                } else {
                    if (debug_level > 0)
                        fprintf(stderr, "%s: Warn: Failed to open %s (%s)\n",
                                __func__, iface_linkspeed_file,
                                strerror(errno));
                }

                if (strcmp(linkspeed_str, "10\n") == 0 ||
                    strcmp(linkspeed_str, "100\n") == 0 ||
                    strcmp(linkspeed_str, "1000\n") == 0) {
                    linkspeed = atoi(linkspeed_str);
                }

                if (debug_level > 1)
                    fprintf(stderr, "%s: Info: eth0 linkspeed (rx/tx): "\
                                    "%d Mbit/s\n", __func__, linkspeed);

                extd_statenum = linkspeed;
            }

        } else { /* iface_name == "wlan0 " */

            char *wlan0_speedcmd = WLAN0SPEEDCMD;
            FILE *fd_cmdbuf;
            char bitrate_str[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};

            if ((fd_cmdbuf = popen(wlan0_speedcmd, "r")) != NULL) {
                if (fgets(bitrate_str, 6, fd_cmdbuf) != NULL) {
                    pclose(fd_cmdbuf);
                    linkspeed = atoi(bitrate_str);
                } else {
                    if (debug_level > 0)
                        fprintf(stderr, "%s: Warn: Failed to parse %s (%s)\n",
                                __func__, wlan0_speedcmd, strerror(errno));
                }
                if (debug_level > 1)
                    fprintf(stderr, "%s: Info: wlan0 bitrate (rx/tx): "\
                                    "%d Mbit/s\n", __func__, linkspeed);

                extd_statenum = linkspeed;
            } else {
                if (debug_level > 0)
                    fprintf(stderr, "%s: Warn: Failed to execute %s (%s)\n",
                                    __func__, wlan0_speedcmd, strerror(errno));
            }
        }
    }

    return extd_statenum;
}

/*!
   The USB bus has one or several built in devices; this command detects if
   there are any removable devices attached.
*/
int usb_getstate(void)
{
    int exists_extusbdev = 0;

    /* Open a stream and receive the response to the USB info command. */
    char *usb_infocmd = USBINFOCMD;
    char usb_removables[BUFFERSIZE];
    FILE *fd_cmdbuf;
    if ((fd_cmdbuf = popen(usb_infocmd, "r")) != NULL) {
        if (fgets(usb_removables, BUFFERSIZE, fd_cmdbuf) != NULL) {
            pclose(fd_cmdbuf);
            exists_extusbdev = 1;
        } else {
            if (debug_level > 0)
                fprintf(stderr, "%s: Warn: Failed to parse %s (%s)\n",
                                __func__, usb_infocmd, strerror(errno));
        }
        if (debug_level > 1)
            fprintf(stderr, "%s: Info: usb removable devices: \n%s",
                             __func__, usb_removables);
    } else {
        if (debug_level > 0)
            fprintf(stderr, "%s: Warn: Failed to execute %s (%s)\n",
                             __func__, usb_infocmd, strerror(errno));
    }

    return exists_extusbdev;
}

/*! Reads the temperature from a sysfs node. */
int temp_getstate(void)
{
    char *temp_file = TEMPSTATEFILE;
    FILE *fd_temp = fopen(temp_file, "r");
    /* Form of temperature string is "51000\n" for 51 degC. */
    char temp_str[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};

    if (fd_temp != NULL) {
        if (fgets(temp_str, 6, fd_temp) != NULL) {
            fclose(fd_temp);
        } else {
            if (debug_level > 0)
                fprintf(stderr, "%s: Warn: Failed to read %s (%s)\n",
                                __func__, temp_file, strerror(errno));
        }
    } else {
        if (debug_level > 0)
            fprintf(stderr, "%s: Warn: Failed to open %s (%s)\n",
                            __func__, temp_file, strerror(errno));
    }

    int temp_degc = mystr2num(temp_str)/1000;

    if (debug_level > 1)
            fprintf(stderr, "%s: Info: Temperature is %d deg C\n",
                            __func__, temp_degc);

    return temp_degc;
}
