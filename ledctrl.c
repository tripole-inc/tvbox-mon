/*!\file
    The functions here perform all the low level icon/LED control functions,
    such as turning the icon on or off, blink it or do a ramp up-hold-down.
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "def.h"
#include "ledctrl.h"
#include "utility.h"

extern int debug_level;

/*!
    The basic icon/LED control function; turn the icon on or off. The icon
    state is maintained by the variable indc_state (value kept in main()).
*/
int led_onoff(int onoff, int *indc_state, char *indc_icon,
              FILE *fd_ledon, FILE *fd_ledoff)
{
    int worktime_musec = 0;
    char ctrl_str[VFDICONNAMLEN+1];

    /* On/off exec. time is effectively determined by vfdnap() (see below). */

    strcpy(ctrl_str, &indc_icon[0]);
    strcat(ctrl_str, "\n");

    int ret = 0; 
    if (onoff == 1) {
        ret = fputs(ctrl_str, fd_ledon);
    }
    else {
        ret = fputs(ctrl_str, fd_ledoff);
    }

    if (ret == -1) {
        if (debug_level > 0)
            fprintf(stderr, "%s: Error: Failed to write to indicator"\
                            "led node %d (%s)\n",
                            __func__, ret, strerror(errno));

        exit(1);
    }
    else {
        if (debug_level > 1) {
            if (onoff == 1) {
                fprintf(stderr, "%s: Info: Wrote \"%s\\n\" to node %s\n",
                        __func__, &indc_icon[0], OPENVFDPATH"/led_on");
            }
            else {
                fprintf(stderr, "%s: Info: Wrote \"%s\\n\" to node %s\n",
                        __func__, &indc_icon[0], OPENVFDPATH"/led_off");
            }
        }
    }

    /* For VFD displays a nap is required due to the rather slow response time
       of the device but for LEDs this is not needed. However it seems that
       OpenVFD has incorporated a rather slow polling cycle time (500ms) which
       effectively sets a limit here. */
    worktime_musec = vfdnap();

    return worktime_musec;
}

/*!
    Makes the icon/LED blink one or several times with 50% duty cycle.
*/
int led_blink(int noblinks, int onoff, int *indc_state, char *indc_icon,
              FILE *fd_ledon, FILE *fd_ledoff)
{
    int led_state1 = 1, led_state2 = 0;
    int worktime_musec = 0;

    if (onoff == 0) {
        led_state1 = 0;
        led_state2 = 1;
    }

    /* A blink is defined as an on-off (or off-on) operation with equal
       amounts of time on/off (i.e. 50% duty cycle). */

    for (int i = 1; i <= noblinks; i++) {
        worktime_musec +=
            led_onoff(led_state1, indc_state, indc_icon, fd_ledon, fd_ledoff);

        worktime_musec +=
            led_onoff(led_state2, indc_state, indc_icon, fd_ledon, fd_ledoff);
    }

    return worktime_musec;
}

/*! Turn on or off all icons on the display except the colon. */
int icons_onoff(int onoff, struct vfd_iconprops *vfd_icn,
                FILE *fd_ledon, FILE *fd_ledoff)
{
    int worktime_musec = 0;
    struct vfd_iconprops vfd_loc;

    vfd_loc = *vfd_icn;

    worktime_musec += led_onoff(onoff, &vfd_loc.eth_state, vfd_loc.eth_str,
                                fd_ledon, fd_ledoff);
    worktime_musec += led_onoff(onoff, &vfd_loc.wifi_state, vfd_loc.wifi_str,
                               fd_ledon, fd_ledoff);
    worktime_musec += led_onoff(onoff, &vfd_loc.usb_state, vfd_loc.usb_str,
                               fd_ledon, fd_ledoff);
    worktime_musec += led_onoff(onoff, &vfd_loc.play_state, vfd_loc.play_str,
                                fd_ledon, fd_ledoff);
    worktime_musec += led_onoff(onoff, &vfd_loc.pause_state, vfd_loc.pause_str,
                                fd_ledon, fd_ledoff);
    worktime_musec += led_onoff(onoff, &vfd_loc.alarm_state, vfd_loc.alarm_str,
                                fd_ledon, fd_ledoff);

    return worktime_musec;
}
