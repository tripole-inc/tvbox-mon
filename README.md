# TVbox-mon

`TVbox-mon` is a program to enable status indication of system resources using
(icons on) the VFD (vacuum fluorescent) display (or LED display) on a TV-box
running Linux.
Currently, the program can monitor network interfaces (eth0 and wlan0) and
removable devices on the USB bus as well as the temperature of the
system-on-a-chip (SoC).

The display is controlled via the OpenVFD (a.k.a. Linux OpenVFD, a fork of
linux_openvfd) interface in sysfs so the OpenVFD driver/module must be
available.

The code was developed on a Tanix TX3-mini (see main.c) but should be easy to
adapt also to other devices. (A download link for binaries is given at the end
of the document.)
It should also be easy to modify and extent the functionality to include e.g.
disk usage warning.
You are cordially invited and encouraged to fork and modify.

Building
--------

The program can be built (e.g. by cross compilation) using a standard build
environment (including Linux kernel headers) to produce a dynamically linked
binary. A makefile is provided for this, as well as to produce html
documentation for the code.

The program can be built on Ubuntu 20.04 (Focal) x86 with the ARM cross
compiler `aarch64-linux-gnu-gcc` installed by doing

`$ make`

The result should be a binary named `tvbox-mon` of about 35kB size.
To (optionally) build the html documentation, if doxygen and graphviz are
installed on the build host, do

`$ make doc`

The documentation will appear under the directory `html` (starting with
`html/index.html`).

(There is also a static build target in the makefile; the resulting binary
will be about 630kB.)

Usage
-----

`TVbox-mon` can be run from command line, e.g. like

`# ./tvbox-mon -c n -p 2500`

The options can also be put in a config file, at a location specified at
compile time (see def.h). (The options should then be put on a single line,
just as the command line argument string `"-c n -p 2500"` in the example here.
The available options are listed in the command line help menu shown below.)

The command line help menu for `TVbox-mon` looks like this:

```
# ./tvbox-mon -h
This is tvbox-mon, version TX3mini-tripole-220410 (binary name tvbox-mon).
- Program for controlling the VFD icons/LEDs on a TV-box to indicate status. -
Location of (optional) config file: /usr/local/etc/tvbox-mon.conf
If no options are given, the conf. file (if it exists) will be parsed for opts.

Usage (<i> is an integer and <c> is a character):
 tvbox-mon [-h] [-v <i>] [-p <i>] [-c <c>]

Options (default values are used in place of missing or incorrect options):
  -h Help (display this message)
  -v Verbosity level, integer;
     0: silent, 1: errors + warnings (default), 2: errors + warnings + info
  -p Period for one prog. cycle in millisecs., integer (default 500, min 500)
  -c Code for system monitoring program cycle, single character;
     n : network interface status (on/off VFD icons for up/down of eth0, wlan0)
     u : USB bus connections (on/off VFD icon for removable USB devices)
     b : both network interface status and USB bus connections (n and u)
     q : quiet (all VFD icons off)
  Temperature alarm options (to 'disable' alarm, set both temps high);
  -w Warning temp (deg C) for alarm (blinking), integer
  -d Danger temp (deg C) for alarm (constant), integer

Example: ./tvbox-mon -p 2500 -c n -w 70 -d 80
```

**On privileges:** `tvbox-mon` must be run as root, or at least with enough
privileges to have write access to the led_on/led_off nodes in the sysfs
interface for OpenVFD. These nodes are located under `/sys/class/led/openvfd`
when the OpenVFD module is loaded in the kernel.

**On systemd:** There is a template available for a systemd service definition
file so that `tvbox-mon` can be started and managed by systemd.

License
-------

The code for `TVbox-mon` is released under the Gnu General Public License v3.0,
see the file LICENSE.

Documentation
-------------

All parts of the code are documented with doxygen comments and a good overview
of the structure can be obtained by browsing the html docs (which also include
call and caller graphs for the functions).

Additional features
-------------------

The code is modular and it should be straightforward to add features or
modifying the behavior of the program (like those mentioned above).
In particular, adapting it to VFD displays with different icons should be easy.

Download
--------

Compiled binaries of `TVbox-mon` can be found
[here](https://mega.nz/folder/3Aly0QCS#SQ_QVuly6R5YstgQ8oyd4Q).

Disclaimer
----------

The code runs well on my TX3-mini (s905w SoC) but I have no idea about your
box. Use at your own risk!
