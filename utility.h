/*!\file
    Utility functions.
*/

#ifndef UTILITY_H
#define UTILITY_H

/*! Very simple number-to-string converter.*/
int mystr2num(char *numstr);

/*! Short sleep needed for openfd sysfs node polling. */
int vfdnap(void);

#endif
