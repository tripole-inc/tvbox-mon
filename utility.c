/*!\file
    The functions here perform certain simple auxiliary tasks, such as linear
    interpolation of a tabulated function.
*/

#include <string.h>
#include <unistd.h>

#include "def.h"
#include "utility.h"

/*! A very simple number-to-string converter. */
int mystr2num(char *numstr)
{
    int resnum = 0, len = strlen(numstr);

    for (int i=0; i<len; i++)
        resnum = resnum *10 +(numstr[i] -'0');

    return resnum;
}

/*! Short sleep used for openfd sysfs node polling. */
int vfdnap(void)
{
    int naptime_musec = 1000*VFDSLEEPMISEC;

    usleep(naptime_musec);

    return naptime_musec;
}
