/*!\file
  This file contains almost all constants and macros used throughout the code.
  The only exceptions are a few constants (parameters) that are defined at the
  top in main.c.
*/

#ifndef DEF_H
#define DEF_H

/* -------------------------------- Macros ----------------------------------*/

/* We all love stackoverflow. */
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/* ------------------------------- Constants ------------------------------- */

/*! The prefix "mu" is micro- and "mi" is milli- here. */
#define ICONSDEFAULTEXTERNSTATE 0  /*!< {0,1} On/off */
#define VFDDEFAULTCYCLECODE 'h'    /*!< {chr} ascii value of a char  */
#define VFDSLEEPMISEC 500          /*!< {int} Min delay for openvfd polling. */
#define VFDMINPERIODMISEC 500      /*!< {int} Min time for one status loop.  */
#define VFDDFLTPERIODMISEC 2500    /*!< {int} Default time for one loop. */

/*! {int}
   Max number of args. in cmd. line, excl. calling cmd. name (max argc-1). */
/* Note: For example, the call "./tvbox-mon -v 1 -p 2500" has 5 options*/
#define MAXARGWORDS 15

/*! {int}
  Max length of a command line argument +one null, e.g. 5 (for "2500"). */
#define MAXARGSTRLEN 7

/*! {string} Info msg. displayed with option -h (and invalid options). */
#define SYNOPSISSTR "- Program for controlling the VFD icons/LEDs on a "\
                    "TV-box to indicate status. -"

/*! Information strings used in the information message at the help menu. */
#define NETWORKIFACESTATUS "network interface status"      /*!< {string} */
#define USBBUSSTATUS       "USB bus connections"           /*!< {string} */

/*! {string} Info msg. displayed with option -h (and invalid options). */
#define USAGEMSG "Usage (<i> is an integer and <c> is a character):\n %s "\
                       "[-h] [-v <i>] [-p <i>] [-c <c>] [-w <i>] [-d <i>]\n\n"\
                 "Options (default values are used in place of missing or "\
                       "incorrect options):\n"\
                 "  -h Help (display this message)\n"\
                 "  -v Verbosity level, integer;\n"\
                 "     0: silent, 1: errors + warnings (default), "\
                                 "2: errors + warnings + info\n"\
                 "  -p Period for one prog. cycle in millisecs., integer"\
                       " (default "STR(VFDDFLTPERIODMISEC)\
                           ", min "STR(VFDMINPERIODMISEC)")\n"\
                 "  -c Code for system monitoring program cycle, single "\
                       "character;\n"\
                 "     n : "NETWORKIFACESTATUS\
                           " (on/off VFD icons for up/down of eth0, wlan0)\n"\
                 "     u : "USBBUSSTATUS\
                         " (on/off VFD icon for removable USB devices)\n"\
                 "     b : both "NETWORKIFACESTATUS" and "\
                           USBBUSSTATUS" (n and u)\n"\
                 "     q : quiet (all VFD icons off)\n"\
                 "  Temperature alarm options (to 'disable' alarm,"\
                    " set both temps high);\n"\
                 "  -w Warning temp (deg C) for alarm (blinking), integer\n"\
                 "  -d Danger temp (deg C) for alarm (constant), integer\n\n"\
                 "Example: ./tvbox-mon -p 2500 -c n -w 70 -d 80\n"

/*! This location is also printed with command line/config option -h */
#define CONFFILE "/usr/local/etc/tvbox-mon.conf"

/*!
   The network interface properties are mostly read from (virtual) files in
   sysfs (may not be stable accross kernel releases). For more info, see
   https://www.kernel.org/doc/html/latest/admin-guide/sysfs-rules.html
*/

#define NETSTATEPATH   "/sys/class/net"                        /*!< {string} */
#define ETH0STATEPATH  NETSTATEPATH"/eth0"                     /*!< {string} */
#define WLAN0STATEPATH NETSTATEPATH"/wlan0"                    /*!< {string} */

/*! For the ethernet interface eth0 the up/down property is (here) determined
    by the following two conditions:
    (1) The software interface must be up, which is signaled by a single
        keyword in the file /sys/class/net/eth0/operstate where the possible
        values are {"up", "down", "unknown"}.
    (2) The cable must be connected, which is signaled by a single digit
        character in the file /sys/class/net/eth0/carrier where the possible
        values are {'0', '1'}.
    If and only if both (1) and (2) are satisfied can the interface transmit
    and receive (ethernet) internet traffic, and this is what we mean by saying
    that the "interface eth0 is up".
    (The actual linkspeed can be determined from /sys/class/net/eth0/speed.)

    For the ethernet interface wlan0 the up/down property is (here) determined
    by the following two conditions:
    (1) The software interface must be up, which is signaled by a single
        keyword in the file /sys/class/net/wlan0/operstate where the possible
        values are {"up", "down", "unknown"}.
    (2) The wireless connection must be up (associated) and the radio channel
        be able to transmit and receive (ethernet/internet) data.
    If and only if both (1) and (2) are satisfied can the interface transmit
    and receive (ethernet) internet traffic, and this is what we mean by saying
    that the "interface wlan0 is up".

    Condition (1) above is the same for eth0 and wlan0 and is checked in the
    code by simply reading the contents of the operstate file. Condition (2) is
    similarly handled for eth0 by a simple reading of the carrier file but for
    wlan0 we need to filter the output of a command to get an actual bit rate.

    Since it is easier to write elementary line/string processing code in
    shell script, and since time is not critical (the openfvd polling delay 
    effectively restricts speed) we do as much preprocessing as possible for
    wlan0 with the shell script command string below. 
 */

/*! {string} The wlan0 link speed is based on data from this command. */
#define CUTBIN "/usr/bin/cut"                                  /*!< {string} */
#define IWCONFBIN "/usr/sbin/iwconfig"                         /*!< {string} */
#define WLAN0SPEEDCMD IWCONFBIN" wlan0 | grep 'Bit Rate' | "CUTBIN\
                      " -d ':' -f 2 | "CUTBIN" -d ' ' -f 1"    /*!< {string} */

/*! Command to get usb bus info. In usb_action() the presence of removable usb
    devices is equivalent to a non empty string (not counting \n) as output to
    the USBINFOCMD specified below. */
#define LSUSBBIN "/usr/bin/lsusb"                              /*!< {string} */
#define GREPBIN  "/usr/bin/grep"                               /*!< {string} */
#define USBEXCLUDE "'Linux Foundation'"                        /*!< {string} */
#define USBINFOCMD LSUSBBIN" | "GREPBIN" -v "USBEXCLUDE        /*!< {string} */

/*! Location of temperature reading node in sysfs. */
#define TEMPSTATEPATH "/sys/class/thermal/thermal_zone0"       /*!< {string} */
#define TEMPSTATEFILE TEMPSTATEPATH"/temp"                     /*!< {string} */

/*! Default values for temp alarm, can be overridden w/ command line args. */
#define WARNINGTEMP 70 /*! {int} Degrees centigrade for blinking alarm. */
#define DANGERTEMP  80 /*! {int} Degress centigrade for constant alarm. */

#define IFACENAMELEN 10 /*!< {int} For various buffers holding iface names.  */

#define BUFFERSIZE 300  /*!< {int} For reading network data from proc/       */
#define LINELEN 80      /*!< {int} Length of a line (of char.); for buffers. */

/*! Path in sysfs to the openvfs led (on/off) nodes. */
#define OPENVFDPATH "/sys/class/leds/openvfd"                  /*!< {string} */

/*! Max length of strings to hold names of the vfd icons (incl. null char). */
#define VFDICONNAMLEN 10                                           /*!< {int}*/

/*! Default name of icon on VFD to be used as indicator (blinking) indicator.
    The available names/icons depend on the type of display, see the docs for
    openvfd (e.g. the openvfd configuration file for the display). */
#define INDICICON "play"                                       /*!< {string} */

/*! Version info. */
#define PROGNAMESTR "\e[36mtvbox-mon\e[39m\e[0m" /*!< {string} (w/ color) */
#define VERSIONSTR "TX3mini-tripole-220404"      /*!< {string} */

/* -------------------------------- Typedefs ------------------------------- */

/*! Icon descriptor strings, according to OpenVFD nomenclature, and associated
    icon/LED states on the display. */
struct vfd_iconprops {
    char eth_str[VFDICONNAMLEN];   /*!< {string} Name of eth icon. */
    char wifi_str[VFDICONNAMLEN];  /*!< {string} Name of wifi icon. */
    char usb_str[VFDICONNAMLEN];   /*!> {string} Name of usb icon. */
    char play_str[VFDICONNAMLEN];  /*!> {string} Name of play icon. */
    char pause_str[VFDICONNAMLEN]; /*!> {string} Name of play icon. */
    char alarm_str[VFDICONNAMLEN]; /*!> {string} Name of alarm icon. */
    char colon_str[VFDICONNAMLEN]; /*!> {string} Name of colon. */
    int eth_state;   /*!< {0,1} On/off */
    int wifi_state;  /*!< {0,1} On/off */
    int usb_state;   /*!< {0,1} On/off */
    int play_state;  /*!< {0,1} On/off */
    int pause_state; /*!< {0,1} On/off */
    int alarm_state; /*!< {0,1} On/off */
    int colon_state; /*!< {0,1} Constant or blinking (1s) icon. */
};

#endif
